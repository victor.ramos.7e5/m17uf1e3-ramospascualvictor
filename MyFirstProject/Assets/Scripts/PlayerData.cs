using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovingState
{
    Moving,
    Stopped
}
public enum RolJugador
{
    Tank,
    Defensa,
    DPS,
    Support,
}







public class PlayerData : MonoBehaviour
{
    public enum GiantState
    {
        NotActive,
        ProcState,
        Active,
        Cooldown
    }
    public struct PlayerValues
    {

        public float giantStateDuration;
    }
    public RolJugador player_rol;
    public string PlayerName;
    public float Height =1.50f;
    public int[] Stats;
    public Sprite[] _sprite;
    private SpriteRenderer _sr;
    private int _frameIndex;
    private int _count;
    [SerializeField]
    private int _frameRate;
    
    private float defaultpeso; 
    public float speed;
    public float peso;
    private float finalspeed;

    public float distancia;
    private bool miraizq = true;


    public Sprite[] Movement;
    public int Position = 0;
   

    private Transform theTransform;
    public Vector2 Hrange = Vector2.zero;
    public Vector2 Vrange = Vector2.zero;


    private bool _isPlayerWaiting = false;
    private float _waitTime;
    private float _speedSense = -1;
    private float _distanceTravelled;
    private float _waitTimeWhenDistanceAchieved;
    public float distanceToTravel;

    private float minDistanceToTravel = 1f;

    public float giantStateDuration;

    void Distancerank()
    {
        theTransform.position = new Vector3(Mathf.Clamp (transform.position.x,Hrange.x,Vrange.y),
            Mathf.Clamp(transform.position.y, Hrange.x, Vrange.y),transform.position.z);
    }
    // Start is called before the first frame update
    void Start()
    {
        theTransform = GetComponent<Transform>();
        _frameRate = 12;

        distanceToTravel = 10;
        _distanceTravelled = 0;
        _waitTimeWhenDistanceAchieved = 1;
        _waitTime = 0;


        defaultpeso = 20;
        _sr = GetComponent<SpriteRenderer>();

        
        
        _count++;
        finalspeed = ((defaultpeso * (speed / 100)) / peso);
    }

    // Update is called once per frame
    void Update()
    {
        
       
        //finalspeed = ((defaultpeso * (speed / 100)) / peso);
        // MovimentoAuto(finalspeed);
        Movimiento();
        Distancerank();
        if (minDistanceToTravel <= 0) distanceToTravel = minDistanceToTravel;

        if (_count % _frameRate == 0)
        {
            _sr.sprite = _sprite[_frameIndex];
            _count = 0;
            _frameIndex = _frameIndex < _sprite.Length - 1 ? (_frameIndex + 1) : 0;
        }

        _count++;
    }

    public void Movimiento()
    {
        float imputMovHori = Input.GetAxis("Horizontal");
        

        MovimentoAuto(finalspeed);
        transform.position = new Vector3(finalspeed * imputMovHori + transform.position.x, transform.position.y,transform.position.z);

        Orientacion(imputMovHori);
       
    }

    
    public void MovimentoAuto(float speed)
    {
        if (_isPlayerWaiting)
        {
            _waitTime += Time.deltaTime;
            if (_waitTime >= _waitTimeWhenDistanceAchieved)
            {
                _speedSense *= -1;
                _distanceTravelled = 0;
                _waitTime = 0;
                _isPlayerWaiting = false;
            }
        }
        else
        {
            transform.position = new Vector3(
              transform.position.x + finalspeed * _speedSense * Time.deltaTime,
              transform.position.y);
            _distanceTravelled += finalspeed * Time.deltaTime; // Does not work properly if player collides in position.x
            _isPlayerWaiting = DistanceCheck();
        }

    }
    private bool DistanceCheck()
    {
        if (_distanceTravelled >= distanceToTravel)
            return true;

        return false;
    }

    public float GetMinDistanceToTravel()
    {
        return minDistanceToTravel;
    }


    void Orientacion(float inputmov)
    {
        if ((miraizq ==true && inputmov>0) || (miraizq==false && inputmov<0))
        {
            miraizq = !miraizq;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }


    public float GetGiantStateDuration()
    {
        return giantStateDuration;
    }





}
