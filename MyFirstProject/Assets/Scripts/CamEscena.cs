using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CamEscena : MonoBehaviour
{
    [SerializeField]
    private Text _inputField;
    [SerializeField]
    private Button _startGameButton;
    public static string nombre;

    // Start is called before the first frame update
    void Start()
    {
        Button btn = _startGameButton.GetComponent<Button>();
        btn.onClick.AddListener(LoadScene);
    }

    void LoadScene()
    {
        SceneManager.LoadScene("GameScene");
    }
    void Update()
    {
        nombre = _inputField.text;
    }


}
