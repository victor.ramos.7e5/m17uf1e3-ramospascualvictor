using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI : MonoBehaviour
{
    public PlayerData _playerdata;
    private GameObject _player;
   // public TextGenerator text;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        // _player = GameObject.FindGameObjectsWithTag("Player");
        _playerdata = _player.GetComponent<PlayerData>();
        this.gameObject.GetComponent<Text>().text = _playerdata.name;
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
