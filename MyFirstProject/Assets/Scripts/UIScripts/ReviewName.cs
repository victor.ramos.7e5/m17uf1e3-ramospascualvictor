using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReviewName : MonoBehaviour
{

    private GameObject _player;
    private PlayerData _playerdata;
    private string _name;


    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _playerdata = _player.GetComponent<PlayerData>();
        _name = CamEscena.nombre;
    }

    // Update is called once per frame
    void Update()
    {
        _playerdata.PlayerName = _name;
        this.gameObject.GetComponent<Text>().text = _playerdata.PlayerName;
    }
   

}
