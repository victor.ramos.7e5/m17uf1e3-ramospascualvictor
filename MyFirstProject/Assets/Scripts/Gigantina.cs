using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Gigantina : MonoBehaviour
{

    private PlayerData _values;
    private PlayerData.GiantState _giantState;
    private float _giantStateTimer;
    private float _giantStateDuration;

    // Start is called before the first frame update
    void Start()
    {
        _giantState = PlayerData.GiantState.NotActive;
        _giantStateTimer = 0;
        _values = gameObject.GetComponent<PlayerData>();

    }

    // Update is called once per frame
    void Update()
    {
        _giantStateDuration = _values.GetGiantStateDuration();
        UpdateGiantState();

        if (Input.GetKeyDown(KeyCode.LeftShift) && _giantState == PlayerData.GiantState.NotActive)
            _giantState = PlayerData.GiantState.ProcState;

    }
    private void UpdateGiantState()
    {
        switch (_giantState)
        {
            case PlayerData.GiantState.ProcState:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer < _values.peso)
                    transform.localScale += new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime);
                else
                {
                    _giantStateTimer = 0;
                    _giantState = PlayerData.GiantState.Active;
                }
                break;
            case PlayerData.GiantState.Active:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer >= _giantStateDuration)
                {
                    _giantStateTimer = 0;
                    _giantState = PlayerData.GiantState.Cooldown;
                }
                break;
            case PlayerData.GiantState.Cooldown:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer < _values.peso)
                    transform.localScale -= new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime);
                if (_giantStateTimer >= 30)
                {
                    _giantStateTimer = 0;
                    _giantState = PlayerData.GiantState.NotActive;
                }
                break;
        }
    }

}
